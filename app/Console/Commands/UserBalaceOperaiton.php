<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;

class UserBalaceOperaiton extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user_operation:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add operation on user balance';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $credentials['email'] = $this->ask('Введите email пользователя');
        $credentials['password'] = $this->secret('Введите пароль');
        if (!Auth::attempt($credentials)) {
            return $this->error('Введенный email не существует или не правильный пароль');
        }
        $user = Auth::user();
        $balance = $user->balance;
        $operationName = $this->ask('Введите наименование операции');
        $operationSum = $this->ask('Введите сумму операции');
        if (($balance->balance += (int)$operationSum) < 0) {
            $this->error('Операция не может быть проведена, из-за недостатка средств на балансе пользователя');
            return 0;
        } else {
            $balance->save();
        }
    }
}
