<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserAdd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add user';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $name = $this->ask("User name");
        if (gettype($name) !== 'string') {
            $this->error('Invalid user Name');
        }
        $email = $this->ask('User email');
        if (gettype($email) !== 'string') {
            $this->error('Invalid user Name');
        }
        $password = $this->ask('User password');
        if (gettype($password) !== 'string') {
            $this->error('Invalid user Name');
        }
        try {
            $user = User::create([
                'name' => $name,
                'email' => $email,
                'password' => Hash::make($password)
            ]);
            $this->info('The user created successfully: ' . json_encode($user));
        } catch (\Error $e) {
            $this->warn($e);
        }

    }
}
