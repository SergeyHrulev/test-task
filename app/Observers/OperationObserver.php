<?php

namespace App\Observers;

use App\Models\Operation;
use App\Models\User;

class OperationObserver
{
    /**
     * Handle the Operation "created" event.
     *
     * @param  \App\Models\Operation  $operation
     * @return void
     */
    public function created(Operation $operation)
    {
        $userBalance = User::find($operation->user_id)->balance;
        $userBalance['balance'] += (int)$operation->sum;
        $userBalance->save();
    }

    /**
     * Handle the Operation "updated" event.
     *
     * @param  \App\Models\Operation  $operation
     * @return void
     */
    public function updated(Operation $operation)
    {
        //
    }

    /**
     * Handle the Operation "deleted" event.
     *
     * @param  \App\Models\Operation  $operation
     * @return void
     */
    public function deleted(Operation $operation)
    {
        //
    }

    /**
     * Handle the Operation "restored" event.
     *
     * @param  \App\Models\Operation  $operation
     * @return void
     */
    public function restored(Operation $operation)
    {
        //
    }

    /**
     * Handle the Operation "force deleted" event.
     *
     * @param  \App\Models\Operation  $operation
     * @return void
     */
    public function forceDeleted(Operation $operation)
    {
        //
    }
}
