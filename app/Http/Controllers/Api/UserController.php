<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserDataResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __invoke($limit = 5)
    {
        $userData = User::findOrFail(1);
        $userData->load('balance');
        $userData->load(['operation' => function ($q) use ($limit){
            $q->orderBy('created_at', 'DESC')
                ->limit($limit);
        }]);
        return new UserDataResource($userData);
    }
}
