<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Filters\OperationFilter;
use App\Http\Resources\UserOperationResource;
use App\Models\Operation;
use Illuminate\Http\Request;

class UserOperationController extends Controller
{
    public function __invoke(OperationFilter $filter, $paginator = 15)
    {
        return UserOperationResource::collection(Operation::filter($filter)->paginate($paginator));
    }
}
