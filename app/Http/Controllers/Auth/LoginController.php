<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginUserRequest;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;

class LoginController extends Controller
{
    public function __invoke(LoginUserRequest $request)
    {
        if (!$token = auth()->attempt($request->only('email', 'password'))) {
            return response('Неудачная попытка входа.', 401);
        }
        Cookie::queue('BEARER-TOKEN', json_encode(compact('token')), 36000);
        return redirect('main');
    }
}
