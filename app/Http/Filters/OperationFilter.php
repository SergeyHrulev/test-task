<?php

namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;

class OperationFilter extends QueryFilter
{
    /**
     * @param string $orderDirection
     * @return void
     *
     * Фильтрация по дате
     */
    public function direction(string $direction): void
    {
        $this->builder->orderBy('created_at', $direction);
    }

    /**
     * @param string $description
     * @return void
     *
     * Поиск по описанию операции
     */
    public function description(string $description): void
    {
        $str = trim($description);
        $this->builder->where('description', 'iLike', "%$str%");
    }


}
