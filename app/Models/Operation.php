<?php

namespace App\Models;

use App\Http\Filters\Filterable;
use App\Http\Filters\QueryFilter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Operation extends Model
{
    use HasFactory, Filterable;

    protected $fillable = [
      'user_id',
      'description',
      'sum'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
