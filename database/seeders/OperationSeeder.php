<?php

namespace Database\Seeders;

use Faker\Core\Number;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OperationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 20; $i++) {
            DB::table('operations')->insert([
                'user_id' => 1,
                'description' => 'Операция_№_' . $i,
                'sum' => mt_rand(-500, 1000),
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
