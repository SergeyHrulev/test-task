const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js').vue({version: 2})
    .sass('resources/sass/app.scss', 'public/css', []);

module.exports = {
    module: {
        rules: [
            // ... другие правила опущены

            // это правило будет применяться к обычным файлам `.scss`
            // А ТАКЖЕ к секциям `<style lang="scss">` в файлах `.vue`
            {
                test: /\.scss$/,
                use: [
                    'vue-style-loader',
                    'css-loader',
                    'sass-loader'
                ]
            }
        ]
    },
    // плагин опущен
}
